const express = require('express');
const app = express()
const passport = require('passport');
const passportLocal = require('passport-local');
const passportLocalMongoose = require('passport-local-mongoose');
const slug = require('slug');

const registorRoutes = require("./api/routes/registration")
const adminRouts = require("./api/routes/admin")
const userRouts = require("./api/routes/user")
var cookieParser = require('cookie-parser');

const mongoose = require('mongoose');

//body parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
  useUnifiedTopology: true,
  extended: true
}));

//view enginen
app.set("view engine", "ejs");
app.use(express.static('public'))

app.use(cookieParser());
//Admin schema
var Admin_schema = mongoose.Schema({
  name: {
    type: String,
    require: true
  },
  email: String,
  password: String,
  slug_name: {
    type: String,
    unique: true
  }
})

const uri = "mongodb+srv://milan:milan123456@cluster0-wed9y.mongodb.net/test?retryWrites=true&w=majority";
//for database mongoose connect
mongoose.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useFindAndModify: false
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection-error'));
db.once('open', () => {
  console.log('database connection succesfully');
});

app.use(require('express-session')({
  secret: "my name is milan",
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize());
app.use(passport.session());

Admin_schema.plugin(passportLocalMongoose)
const Admin_Data = mongoose.model('Admin_Data', Admin_schema);


passport.use(new passportLocal(Admin_Data.authenticate()));
passport.serializeUser(Admin_Data.serializeUser());
passport.deserializeUser(Admin_Data.deserializeUser());

Admin_schema.plugin(passportLocalMongoose);

var url = "This is a Nice Car...^^:";

console.log(slug(url));

app.use(registorRoutes)
app.use(adminRouts);
app.use(userRouts);


app.get('*', (req, res) => {

  return res.render("404");
});

const PORT = process.env.PORT || 3010;
app.listen(PORT,(req,res)=>{
    console.log(`server started in port on ${PORT}...`);
})