var mongoose = require("mongoose");
const ObjectId = require('mongodb').ObjectID;
mongoose.set('useCreateIndex', true);


//Blog schema
var Blog_schema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  category: {
    type: ObjectId,
    ref: "Category_Data"
  },
  description: String,
  date: {
    type: Date,
    default: Date.now()
  },
  imageURL: String,
  slug_title: {
    type: String,
    unique: true
  }
})


//category schema
var Category_schema = mongoose.Schema({
  category: {
    type: String,
    required: true
  },
  slug_category: {
    type: String,
    unique: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
})

const Category_Data = mongoose.model('Category_Data', Category_schema);
const Blog_Data = mongoose.model('Blog_Data', Blog_schema);

module.exports = {
  Category_Data,
  Blog_Data,


}