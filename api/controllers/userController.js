var models = require("../schema");
var mongoose = require("mongoose");
var Blog_Data = models.Blog_Data;
var Category_Data = models.Category_Data;
const ObjectId = require('mongodb').ObjectID;
var flag = false;


/**
 * @description to render index page with blogs and categories
 */
exports.renderIndex = async (req, res) => {

  flag = false
  var search = req.query.search;
  var categoryId = req.query.categoryId;
  let blogId = req.query.blogId;

  if (search) {

    var blogs = await Blog_Data.find({
        title: {
          $regex: '.*' + search + '.*',
          $options: 'i'
        }
      })
      .populate("category")
      .sort({
        'date': -1
      })

      .catch(err => {
        console.log(err);
      })


    let categories = await Category_Data.find({
      isDeleted: false
    })

    return res.render("index", {
      datas: blogs,
      cdatas: categories,
      search: search


    });
  } else {
    var where = {}

    if (categoryId) where.category = mongoose.Types.ObjectId(categoryId);

    if (blogId) where._id = mongoose.Types.ObjectId(blogId);

    let blogs = await Blog_Data.find(where)
      .populate("category")
      .sort({
        'date': -1
      })

      .catch(err => {
        console.log(err);
      })

    let categories = await Category_Data.find({
      isDeleted: false
    })

    return res.render("index", {
      datas: blogs,
      cdatas: categories,
      search: search
    });
  }
}


exports.getBlogDetails = async (req, res) => {

  flag = false;

  let blog = await Blog_Data.find({
      slug_title: req.params.slug
    }).populate("category")
    .sort({
      'date': -1
    })

    .catch(err => {
      console.log(err);
    })

  let categories = await Category_Data.find({
    isDeleted: false
  })

  if (blog.length == 0) {
    res.redirect("/404")

  } else {
    return res.render("Usearched", {
      datas: blog,
      cdatas: categories,
    });
  }
}

exports.getCategoryDetails = async (req, res) => {

  flag = false;
  var slug_category = req.params.slug;

  let category = await Category_Data.findOne({
    slug_category: slug_category
  }).catch(err => {
    console.log(err);
  });

  let blog = await Blog_Data.find({
      category: category.id
    }).populate("category")
    .sort({
      'date': -1
    })

    .catch(err => {
      console.log(err);
    })

  let categories = await Category_Data.find({
    isDeleted: false
  });
  return res.render("Usearched", {
    datas: blog,
    cdatas: categories,
  });
}