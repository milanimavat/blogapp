var models = require("../schema");
var mongoose = require("mongoose");
const ObjectId = require('mongodb').ObjectID;
var slug = require('slug');
var Blog_Data = models.Blog_Data;
var Category_Data = models.Category_Data;

exports.adminPage = async (req, res) => {
  flag = true
  var search = req.query.search;
  var categoryId = req.query.categoryId;

  if (search) {

    var blogs = await Blog_Data.find({
        title: {
          $regex: '.*' + search + '.*',
          $options: 'i'
        }
      })

      .populate("category")
      .sort({
        'date': -1
      })
      .catch(err => {
        console.log(err);
      })

    var defaultcategory = await Category_Data.findOne({
      category: "general"
    })

    var categories = await Category_Data.find({})

    return res.render("admin", {
      datas: blogs,
      cdatas: categories,
      gcat: defaultcategory,
      search: search,
      path: '/admin'
    });

  } else {

    var where = {}
    if (categoryId) where.category = mongoose.Types.ObjectId(categoryId);

    if (flag == true) {
      flag = true;

      var blogs = await Blog_Data.find(where)
        .populate("category")
        .sort({
          'date': -1
        })
        .catch(err => {
          console.log(err);
        })

      var defaultcategory = await Category_Data.findOne({
        category: "general"
      })
      var categories = await Category_Data.find({})

      return res.render("admin", {
        datas: blogs,
        cdatas: categories,
        gcat: defaultcategory,
        search: search,
        path: '/admin'

      });
    } else {
      return res.redirect("/");
    }
  }
}

exports.getAllBlogs = async (req, res) => {

  flag = true;
  let search = req.query.search;
  let categoryId = req.query.categoryId;

  if (search) {

    var blogs = await Blog_Data.find({
        title: {
          $regex: '.*' + search + '.*',
          $options: 'i'
        }
      })
      .populate("category")
      .sort({
        'date': -1
      })
      .catch(err => {
        console.log(err);
      })

    var defaultcategory = await Category_Data.findOne({
      category: "general"
    })
    let categories = await Category_Data.find({
      isDeleted: false
    })

    return res.render("allblogs", {
      datas: blogs,
      cdatas: categories,
      gcat: defaultcategory,
      search: search,
      path: '/allblogs'

    });

  } else {

    let where = {}
    if (categoryId) where.category = mongoose.Types.ObjectId(categoryId);

    let blogs = await Blog_Data.find(where)
      .populate("category")
      .sort({
        'date': -1
      })
      .catch(err => {
        console.log(err);
      })

    var defaultcategory = await Category_Data.findOne({
      category: "general"
    })
    let categories = await Category_Data.find({
      isDeleted: false
    })

    return res.render("allblogs", {
      datas: blogs,
      cdatas: categories,
      gcat: defaultcategory,
      search: search,
      path: '/allblogs'

    });
  }
}


exports.searchPageAdmin = async (req, res) => {
  flag = true;
  var search = req.query.search;
  var categoryId = req.query.categoryId;
  var blogId = req.query.slug_title;

  if (search) {

    var blogs = await Blog_Data.find({
        title: {
          $regex: '.*' + search + '.*',
          $options: 'i'
        }
      })
      .populate("category")
      .sort({
        'date': -1
      })
      .catch(err => {
        console.log(err);
      })

    var defaultcategory = await Category_Data.findOne({
      category: "general"
    })

    let categories = await Category_Data.find({
      isDeleted: false
    })

    return res.render("Asearched", {
      datas: blogs,
      cdatas: categories,
      search: search,
      gcat: defaultcategory,
      path: '/asearched'

    });

  } else {

    let where = {}

    if (categoryId) where.category = mongoose.Types.ObjectId(categoryId);

    if (blogId) where.slug_title = blogId;

    let blogs = await Blog_Data.find(where)
      .populate("category")
      .sort({
        'date': -1
      })

      .catch(err => {
        console.log(err);
      })

    let defaultcategory = await Category_Data.findOne({
      category: "general"
    })

    let categories = await Category_Data.find({
      isDeleted: false
    })

    return res.render("Asearched", {
      datas: blogs,
      cdatas: categories,
      search: search,
      gcat: defaultcategory,
      path: '/asearched'

    });
  }
}

exports.form = (req, res) => {
  flag = true;
  var title = req.body.title;
  var categ = req.body.category;
  var description = req.body.dec;
  var date = req.body.date;
  var imageURL = req.body.image;
  var slug_title = slug(title.toLowerCase())
  var fullData = {
    title: title,
    category: mongoose.Types.ObjectId(categ),
    description: description,
    imageURL: imageURL,
    slug_title: slug_title
  }

  var dataAdd = new Blog_Data(fullData);

  dataAdd.save((err, value) => {
    if (err) {
      console.log(`error occur `);
    } else {
      console.log("data stored succesfully")
      return res.redirect('admin');
    }

  })
}

// CATEGORY FORM---------------------------------------------------------------

exports.categoryForm = (req, res) => {
  flag = true;
  var category = req.body.category;
  var slug_category = slug(category.toLowerCase())

  var fullData = {
    category: category,
    slug_category: slug_category
  }
  var dataAdd = new Category_Data(fullData);
  dataAdd.save((err, value) => {
    if (err) {
      console.log(`error occur `);
    } else {
      console.log("category stored succesfully")
    }
  })
  return res.redirect('categories');
}



exports.showcategory = async (req, res) => {

  let search = req.query.search;
  let categoryId = req.query.categoryId;

  let where = {}
  if (search) where.title = search;

  if (categoryId) where.category = mongoose.Types.ObjectId(categoryId);

  let blogs = await Blog_Data.find(where)
    .populate("category")
    .sort({
      'date': -1
    })

    .catch(err => {
      console.log(err);
    })

  let categories = await Category_Data.find({
    isDeleted: false
  })
  var defaultcategory = await Category_Data.findOne({
    category: "general"
  })

  return res.render("categories", {
    datas: blogs,
    cdatas: categories,
    gcat: defaultcategory,
    search: search,
    path: '/categories'

  });
}

exports.editBlog = async (req, res) => {
  var id = req.query.id;
  let category = await Category_Data.find({
    isDeleted: false
  })

  var data = await Blog_Data.findById(id).populate("category").catch(err => {
    throw err
  })

  let categories = await Category_Data.find({
    isDeleted: false
  })
  var defaultcategory = await Category_Data.findOne({
    category: "general"
  })
  if (data) {
    return res.render('editblog', {
      data,
      gcat: defaultcategory,
      cdatas: categories,

      category
    });
  }
}


exports.editCategory = async (req, res) => {
  flag = false;
  var id = req.query.id;

  let category = await Category_Data.findById({
    _id: id
  })

  let categories = await Category_Data.find({
    isDeleted: false
  })
  var defaultcategory = await Category_Data.findOne({
    category: "general"
  })
  if (category) {
    return res.render('editcategory', {
      gcat: defaultcategory,
      cdatas: categories,

      category: category
    });
  }
}


exports.editblog = async (req, res) => {
  flag = true;

  var categ = req.body.category;
  let category = await Blog_Data.findOneAndUpdate({
    _id: new ObjectId(req.body.id)
  }, {
    $set: {
      title: req.body.title,
      category: mongoose.Types.ObjectId(categ),
      description: req.body.dec,
      imageURL: req.body.imageURL
    }
  }).catch(err => {
    throw err
  })
  return res.redirect('admin')
}

exports.editcategory = async (req, res) => {
  flag = true

  var categ = req.body.category;

  let category = await Category_Data.findOneAndUpdate({
    _id: new ObjectId(req.body.id)
  }, {
    $set: {
      category: categ,
    }
  }).catch(err => {
    throw err
  })

  return res.redirect('categories')
}

exports.deleteBlog = (req, res) => {
  // res.send(req.query.id)
  Blog_Data.deleteOne({
    _id: req.query.id
  }, (err) => {
    if (err) {
      throw err;
    } else {
      return res.redirect('allblogs');
    }
  })
}

exports.deleteCategory = (req, res) => {

  Category_Data.updateOne({
    _id: req.query.id
  }, {
    $set: {
      isDeleted: true
    }
  }, (err) => {
    if (err) {
      throw err;
    } else {
      return res.redirect('categories');
    }
  })
}

exports.adminSlugSearch = async (req, res) => {
  var slug = req.params.slug;
  flag = true;
  var blog = await Blog_Data.find({
      slug_title: req.params.slug
    }).populate("category")
    .sort({
      'date': -1
    })

    .catch(err => {
      console.log(err);
    })

  var categories = await Category_Data.find({
    isDeleted: false
  })
  var defaultcategory = await Category_Data.findOne({
    category: "general"
  })

  if (blog.length == 0) {
    res.redirect("/404")

  } else {
    return res.render("Asearched", {
      datas: blog,
      cdatas: categories,
      gcat: defaultcategory,
      path: '/asearched'

    });

  }
}

exports.adminCategorySlugSearch = async (req, res) => {

  flag = true;
  var slugg_category = req.params.slug;

  let blog = await Blog_Data.find({
      category: slugg_category
    }).populate("category")
    .sort({
      'date': -1
    })

    .catch(err => {
      console.log(err);
    })

  var defaultcategory = await Category_Data.findOne({
    category: "general"
  })

  let categories = await Category_Data.find({
    isDeleted: false
  });

  return res.render("Asearched", {
    datas: blog,
    cdatas: categories,
    gcat: defaultcategory,
    path: '/asearched'

  });
}