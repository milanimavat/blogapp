exports.authenticate = (req, res, next) => {

  if (req.cookies['authenticate'] === "true") {
    next();
  } else {

    return res.redirect("/")
  }
}