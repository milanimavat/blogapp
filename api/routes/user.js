var express = require("express")
var router = express.Router();
const userController = require("../controllers/userController")

router.get("/", userController.renderIndex)
router.get('/detail/:slug', userController.getBlogDetails)
router.get('/category/:slug', userController.getCategoryDetails)

module.exports = router;