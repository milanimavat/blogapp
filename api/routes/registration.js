var express = require("express")
var router = express.Router();
var passport = require("passport")

router.get("/register", (req, res) => {
  res.render("register");
})

// middeleware
router.post("/register", (req, res, next) => {

  // const {username, password} = req.body;
  passport.authenticate('local', function (err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.redirect('/');
    }
    req.logIn(user, function (err) {
      if (err) {
        return next(err);
      }
      res.cookie('authenticate', 'true')

      return res.redirect('/admin');
    });
  })(req, res, next);

})
// router.post("/register", passport.authenticate("local", {
//   successRedirect: "/admin",
//   failureRedirect: "/",
//   failureFlash: true
// }), (req, res) => {
//   console.log("here");

// })


router.get("/category/register", (req, res) => {
  res.render("register");
})

router.get("/logout", (req, res) => {
  res.clearCookie('authenticate');
  res.redirect("/");
})
// middeleware

router.post("/category/register", passport.authenticate("local", {
  successRedirect: "/admin",
  failureRedirect: "/",
  failureFlash: 'Invalid username or password.'
}), (req, res) => {

})

module.exports = router;