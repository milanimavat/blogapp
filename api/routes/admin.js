var express = require("express")
var router = express.Router();

var mongoose = require("mongoose");
mongoose.set('useCreateIndex', true);
const app = express()
const adminController = require("../controllers/adminController")
const authenticateUser = require("../middleware/authenticateUser")

router.get("/admin", authenticateUser.authenticate, adminController.adminPage)
router.get("/allblogs", authenticateUser.authenticate, adminController.getAllBlogs)
router.get("/Ablogs", authenticateUser.authenticate, adminController.searchPageAdmin)
router.post("/form", authenticateUser.authenticate, adminController.form)
router.post("/form2", authenticateUser.authenticate, adminController.categoryForm)
router.get("/categories", authenticateUser.authenticate, adminController.showcategory)
router.get("/editblog", authenticateUser.authenticate, adminController.editBlog)
router.get("/editcategory", authenticateUser.authenticate, adminController.editCategory)
router.post("/editblog", authenticateUser.authenticate, adminController.editblog)
router.post("/editcategory", authenticateUser.authenticate, adminController.editcategory)
router.get("/deleteblog", authenticateUser.authenticate, adminController.deleteBlog)
router.get("/deletecategory", authenticateUser.authenticate, adminController.deleteCategory)
router.get("/admin/:slug", authenticateUser.authenticate, adminController.adminSlugSearch)
router.get("/admin/category/:slug", authenticateUser.authenticate, adminController.adminCategorySlugSearch)

module.exports = router;